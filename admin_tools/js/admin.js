/** Admin.js
 *
 * Javascript support for the admins page.
*/
var accessToken;
var totalNumQueries, totalNumVideos;
/* Array to store the list of videos */
var videoTableRows_;
var page = 0;
// number of videos/queries to display per page
var itemsPerPage = 20;

$(document).ready(function(){
	if (typeof(token) == 'undefined' || !token) {
		alert("error not logged in");
		window.location.href = "/admin";
	} else {
		accessToken = token.access_token;
		initPage();
		$('#addVideoButton').show();
        $('addThumbnailsButton').show();
	}
});
/**
 * Closure function for the page
 */
function initPage() {
	var siteUrl = location.origin; //SCOTT: Change this to the domain name of te website
	// var siteUrl = "http://smartsign.imtc.gatech.edu"; //SCOTT: Change this to the domain name of te website
	// var siteUrl = "https://dictionary-smartsign.rhcloud.com"; //SCOTT: Change this to the domain name of te website

	/* Array to store the list of videos */
	//var videoTableRows_;

	/* Array to store the list of queries */
	var queryTableRows_;

	/* Integer to store the current page number */
    //var page = 0;
    page = 0;

	var DEFAULT_DESC = 'Please visit the Center for Accessible Technology in Sign \nwww.cats.gatech.edu';

	/* Initialize navigation tools */
	$('#queriesContainer').hide();

	initOnclickListener();

	$('#videos').click(function(){
		$('#videos').addClass('active');
		$('#queries').removeClass('active');
		$('#videosContainer').show();
		$('#queriesContainer').hide();
		initVideoOnClickListeners();
		refreshVideoPage(0);
		page = 0;
	});

	$('#queries').click(function(){
		$('#videos').removeClass('active');
		$('#queries').addClass('active');
		$('#videosContainer').hide();
		$('#queriesContainer').show();
		initQueryOnClickListeners();
		refreshQueriesPage(0);
		page = 0;
	});

	$('#videos').click();
	$('#uploadVideoForm').submit(initVideoUpload);
	$('#editUploadForm').submit(submitVideoChanges);
    $('#addThumbnailsForm').submit(addThumbnails);
	$('#uploadProgress').hide();
	$('#addVideoButton').hide();
    $('#addThumbnailsButton').hide();

	$('.switch_user_button').click(function() {
		window.location.href = '/authDirect';
	});

	$(document).on('change', '.btn-file :file', function() {
		  var input = $(this);
		  var label = input.parents('.input-group').find(':text');
		  var labelText = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		  label.val(labelText);
	});


    // var loadQueryResults = function(thePage) {
    var loadQueryResults = function() {
		var table = $('.querylist');
		var window = itemsPerPage;
		var i = 0;

		table.html('');

		while (i < queryTableRows_.length && i < window) {
			var ts = queryTableRows_[i].timestamp;
			var q = queryTableRows_[i].query;
			var ip = queryTableRows_[i].ip_address;
			var hit = queryTableRows_[i].found;

			var d = new Date(ts);
			ts = (d.getMonth()+1) + "/" + d.getDate() + "/" + d.getFullYear() + " " + d.getHours() + ":" + ("0"+(d.getMinutes())).slice(-2);

			var classes = 'label ' + (hit ? 'label-success' : 'label-danger');

			var row = $('<tr>').
				append($('<td>').text(ts)).
				append($('<td>').text(q)).
				append($('<td>').text(ip)).
				append($('<td>').
						append($('<span>').addClass(classes).text(hit)));

			table.append(row);
			i++;
		}

        // if (i == queryTableRows_.length) {
        if (i == totalNumQueries) {
			$('.nav-next').addClass('disabled');
		} else {
			$('.nav-next').removeClass('disabled');
		}

		if (page == 0) {
			$('.nav-prev').addClass('disabled');
		} else {
			$('.nav-prev').removeClass('disabled');
		}
	}

	var loadVideoResults = function() {
	    var numVideos = $('.numVideos');
		var table = $('.videolist');
		var window = itemsPerPage;
		var i = 0;
		var data = videoTableRows_;

        numVideos.html(totalNumVideos);
		table.html('');

		while (i < data.length && i < window) {
			var keywords = $('<td class="tr-keywords">');
			for (var key in data[i].keywords) {
				keywords.append($('<span>')
						.addClass('label label-default keyword')
						.text(data[i].keywords[key]));
			}

			var row = $('<tr id="'+ data[i].id +'">')
                .append($('<td>').text(page * itemsPerPage + i + 1))
                // .append($('<td>').text(i + 1))
                .append('<td><img src = "' + data[i].thumbnail + '" class="img-thumbnail" /></td>')
				.append($('<td class="tr-title">').text(data[i].title))
				.append($('<td>').append(
					$('<a href="https://www.youtube.com/watch?v=' +
						data[i].id + '" target=_blank>').text(data[i].id)))
				.append(keywords)
				.append($('<td>').append($('<center>').append($('<div>')
					.addClass('btn-group').append($('<button type="button" data-index="' + i + '">')
						.addClass('edit-video-btn btn btn-default btn-sm')
							.append($('<span>')
								.addClass('glyphicon glyphicon-pencil')
								)
							).append($('<button type="button">')
							.addClass('delete-video-btn btn btn-danger btn-sm delete').append(
								$('<span>').addClass('glyphicon glyphicon-trash')
							)
						)
					)
				)
			);
			table.append(row);

			i++;
		}

		if (i == totalNumVideos) {
			$('.nav-next').addClass('disabled');
		} else {
			$('.nav-next').removeClass('disabled');
		}

		if (page == 0) {
			$('.nav-prev').addClass('disabled');
		} else {
			$('.nav-prev').removeClass('disabled');
		}

		// custom button implementations
	}

	function refreshVideoPage(thePage) {
		var searchtext = $('.searchtext').val();
		var baseUrl = siteUrl + "/videos";
        var limit = '?limit=' + itemsPerPage + '&skip=' + (thePage * itemsPerPage);

		if (searchtext) {
			baseUrl += "/search/" + searchtext;
            page = thePage;
		}

        baseUrl += limit;

        $.ajax({
			type : "GET",
			url : baseUrl,
			success : function(data) {
                totalNumVideos = data.total;
                videoTableRows_ = data.data;
				// page = 0;
				loadVideoResults();
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert("get videos failed " + errorThrown);
			}
		});
	}

	function refreshQueriesPage(thePage) {
		var searchtext = $('.searchtext').val();
		var baseUrl = siteUrl + '/queries';
		var limit = '?limit=' + itemsPerPage + '&skip=' + (thePage * itemsPerPage);

		if (searchtext) {
			baseUrl += "/search/" + searchtext;
		}

		baseUrl += limit;

		$.ajax({
			type : "GET",
			url : baseUrl,
			success : function(data) {
                totalNumQueries = data.total;
				queryTableRows_ = data.data;
				// page = 0;
				loadQueryResults();
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert("get queries failed " + errorThrown);
			}
		});
	}

	/**
	 * Initialize on clicker button listeners
	 */
	function initOnclickListener() {
        /* click listener for the reset button */
        $("#reset-button").click(function() {
            var msg = "This action will wipe the database and repopulate the " +
                      "fields with data from the YouTube channel.\n\n" +
                      "Completing the action may take some time. Are you sure " +
                      "you want to continue?";

            if (confirm(msg)) {
                requestHardReset();
            }
        });

        /* click listener for the print queries button */
        $("#print-queries-button").click(function(e) {
            // getQueryCSVToPrint();
            var baseUrl = siteUrl + '/queries.csv';

            e.preventDefault();  //stop the browser from following
            window.location.href = baseUrl;
        });

        /* click listener for the print videos button */
        $("#print-videos-button").click(function(e) {
            // getVideoCSVToPrint();
            var baseUrl = siteUrl + '/videos.csv';

            e.preventDefault();  //stop the browser from following
            window.location.href = baseUrl;
        });

        /* click listener for the upload thumbnails button */
        $("#upload-thumbnails-button").click(function() {
            $('#addThumbnailsModal').modal('show');
        });

        /* click listener for the edit video button */
        $("tbody").on("click", ".edit-video-btn", function(event) {
            var button = $(event.target).closest('.btn');
            var index = button.attr('data-index');
  			var row = $(this).closest("tr");
			var videoID = row.attr("id");
			var title = row.find(".tr-title").text();
            var thumbnail = row.find(".img-thumbnail").attr("src");
			//var tags = "";
			var tags = [], i = 0;
			row.find(".keyword").each(function() {
				//tags += $(this).text() + ",";
				tags[i++] = $(this).text();
			});
			tags = tags.toString();
			$("#editID").val(videoID);
			$("#editVideoTitle").val(title);
			$("#editVideoDescription").val(DEFAULT_DESC);
			$("#editVideoTags").val(tags);

            $('#editVideoThumbnail').attr("src", thumbnail);

            $(".edit-privacy input[value='" + videoTableRows_[index].privacyStatus  + "']").closest('.btn').button('toggle');

			$("#editVideoModal").modal("show");
		});

		$("tbody").on("click", ".delete-video-btn", function() {
			var row = $(this).closest("tr");
			var videoID = row.attr("id");
			var title = row.find(".tr-title").text();
			$("#deleteID").val(videoID);
			$("#deleteTitle").val(title);

			$("#deleteVideoModal").modal("show");
		});
		//buttons

		$("#deleteSubmit").click(function() {
			var videoID = $("#deleteID").val();
			var row = $("#" + videoID);
			var button = row.find(".delete-video-btn");
			button.attr('disabled', true);
			$("#deleteVideoModal").modal("hide");
			deleteVideo(videoID, button);
		});

        $('#addVideoButton').on('click', function() {
            $("#videoDescription").val(DEFAULT_DESC);
        });
	}

	function initVideoOnClickListeners() {
		$('.searchtext').val('');
		$('.nav-next').off().click(function() {
			// Only activate the navigation listener if the
			// navigation button is visible.
			if (!$(this).hasClass('disabled')) {
                page = Math.min(page + 1, totalNumVideos / itemsPerPage);
                refreshVideoPage(page);
                // loadVideoResults(page);
			}
		});

		$('.nav-prev').off().click(function() {
			if (!$(this).hasClass('disabled')) {
				page = Math.max(page - 1, 0);
                refreshVideoPage(page);
                // loadVideoResults(page);
			}
		});

		$('.searchtext').off().bind('keypress', function(event) {
			event.stopPropagation();
			if (event.keyCode == 13) {
				event.preventDefault();
				refreshVideoPage(0);
			}
		});
	}

	/**
	 * Initializes the event listeners for the queries page
	 */
	function initQueryOnClickListeners() {
		$('.searchtext').val('');
		$('.nav-next').off().click(function() {
			if (!$(this).hasClass('disabled')) {
                // page = Math.min(page + 20, queryTableRows_.length);
                // page = Math.min(page + 20, totalNumQueries);
				// loadQueryResults(page);
                page = Math.min(page + 1, totalNumQueries / itemsPerPage);
                refreshQueriesPage(page);
			}
		});

		$('.nav-prev').off().click(function() {
			if (!$(this).hasClass('disabled')) {
				// page = Math.max(page - 20, 0);
				// loadQueryResults(page);
                page = Math.max(page - 1, 0);
                refreshQueriesPage(page);
            }
		});

		$('.searchtext').off().bind('keypress', function(event) {
			event.stopPropagation();
			if (event.keyCode == 13) {
				event.preventDefault();
				refreshQueriesPage(0);
			}
		});
	}

	/**
	 * Stub button clicks
	 */
	function stub(buttonId) {
		alert("clicked " + buttonId);
	}

    /**
     * Upload .zip file with custom video thumbnails
     */
    function addThumbnails(e) {
        e.preventDefault();
        clearBootStrapPlaceholder('#thumbnailsErrorPlaceholder');

        var progressbox     = $('#uploadThumbnailsProgress');
        var progressbar     = $('#uploadThumbnailsProgressBar');
        var statustxt       = $('#thumbnailsErrorPlaceholder');
        var completed       = '0%';

        var file = $('#thumbnailsFile').get(0).files[0];
        //    //fd = new FormData(),
        //    baseUrl = siteUrl + '/thumbnails',
        //    ajax;


        if (file) {
            //fd.append("fileToUpload", file);

            /*
            ajax = $.ajax({
                timeout : TIMEOUT_30_SEC,
                url: baseUrl,
                method: 'POST',
                //contentType: file.type,
                xhr: function() {
                    var xhr = $.ajaxSettings.xhr();

                    if (xhr.upload) {
                        xhr.upload.addEventListener(
                            'progress',
                            function(e) {
                                if(e.lengthComputable) {
                                    var percentage = Math.round(100 * e.loaded / e.total);

                                    $('#uploadThumbnailsProgress').show();
                                    $('#uploadThumbnailsProgressBar').attr('data-transitiongoal', percentage);
                                    $('.progress .progress-bar').progressbar({display_text: 'fill'});
                                }
                            },
                            false
                        );
                    }

                    return xhr;
                },
                processData: false,
                data: file
            });
            */
            var options = {
                target:   '#output',   // target element(s) to be updated with server response
                beforeSubmit:  beforeSubmit,  // pre-submit callback
                uploadProgress: onProgress,
                complete: afterSuccess,  // post-submit callback
                error:  onError,
                resetForm: true        // reset the form after successful submit
            };

            //when upload progresses
            function onProgress(event, position, total, percentComplete)
            {
                //Progress bar
                progressbar.show();
                progressbar.attr('data-transitiongoal', percentComplete);
                $('.progress .progress-bar').progressbar({display_text: 'fill'});
            }

            //after succesful upload
            function afterSuccess(response)
            {
                $('#uploadThumbnailsProgress').hide();
                $('#addThumbnailsModal').modal('hide');
                $('#uploadThumbnailsForm').find("input[type=text], textarea").val("");

                $('#uploadThumbnails').attr('disabled', false);
                $('#cancelThumbnailsUpload').attr('disabled', false);
            }

            function onError(xhr) {
                // $('#uploadThumbnails').click(function() {
                alert('Upload thumbnails failed. Error: ' + xhr.status);
                //});


                $('#uploadThumbnails').attr('disabled', false);
                $('#cancelThumbnailsUpload').attr('disabled', false);
            }

            //function to check file size before uploading.
            function beforeSubmit(){
                //check whether browser fully supports all File API
                if (window.File && window.FileReader && window.FileList && window.Blob)
                {

                    if( !$('#thumbnailsFile').val()) //check empty input filed
                    {
                        AddBootStrapDangerAlert('#thumbnailsErrorPlaceholder', 'You must choose a file');
                        return false
                    }

                    var fsize = $('#thumbnailsFile')[0].files[0].size; //get file size
                    var ftype = $('#thumbnailsFile')[0].files[0].type; // get file type

                    //allow only valid file types
                    switch(ftype)
                    {
                        case 'application/zip':
                        case 'application/x-zip-compressed':
                        case 'application/octet-stream' :
                        break;
                        default:
                            AddBootStrapDangerAlert('#thumbnailsErrorPlaceholder', "<b>"+ftype+"</b> Unsupported file type!");
                            return false
                    }

                    //Allowed file size is less than 50 MB (52428800)
                    if(fsize > 52428800)
                    {
                        AddBootStrapDangerAlert('#thumbnailsErrorPlaceholder', "<b>"+bytesToSize(fsize) +"</b> Too big file file! <br />Please reduce the size of your images using an image editor.");
                        return false
                    }

                    //Progress bar
                    /*
                    progressbox.show(); //show progressbar
                    progressbar.width(completed); //initial value 0% of progressbar
                    statustxt.html(completed); //set status text
                    statustxt.css('color','#000'); //initial color of status text


                    $('#submit-btn').hide(); //hide submit button
                    $('#loading-img').show(); //hide submit button
                    $("#output").html("");
                     */
                }
                else
                {
                    //Output error to older unsupported browsers that doesn't support HTML5 File API
                    AddBootStrapDangerAlert('#thumbnailsErrorPlaceholder', "Please upgrade your browser, because your current browser lacks some new features we need!");
                    return false;
                }
            }

            //function to format bites bit.ly/19yoIPO
            function bytesToSize(bytes) {
                var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
                if (bytes == 0) return '0 Bytes';
                var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
                return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
            }


            $(this).ajaxSubmit(options);

            return false;


            /*
             $('#MyUploadForm').submit(function() {
                 $(this).ajaxSubmit(options);
                 // return false to prevent standard browser submit and page navigation
                 return false;
             });

             ajax = $.ajax({
                 url: baseUrl,
                 method: 'POST',
                 contentType: false,
                 processData: false,
                 data: fd
             });

             ajax.done(function(response) {
                 $('#uploadThumbnailsProgress').hide();
                 $('#addThumbnailsModal').modal('hide');
                 $('#uploadThumbnailsForm').find("input[type=text], textarea").val("");

                 $('#uploadThumbnails').attr('disabled', false);
                 $('#cancelThumbnailsUpload').attr('disabled', false);
             });

             ajax.fail(function() {
                // $('#uploadThumbnails').click(function() {
                     alert('Upload thumbnails failed. Please try again later');
                 //});


                 $('#uploadThumbnails').attr('disabled', false);
                 $('#cancelThumbnailsUpload').attr('disabled', false);
             });
             */
        }
        else {
            AddBootStrapDangerAlert('#thumbnailsErrorPlaceholder', 'You must choose a file');
        }

    }

    /**
     * Sends hard reset request to the server.
     */
    function requestHardReset() {
        var baseUrl = siteUrl + "/videoshardreset";

        $.ajax({
            type : "POST",
            url : baseUrl,
            success : function(result) {
                console.log(result);
                AddBootStrapInfoAlert('#videoUploadStatusPlaceholder',
                    'Message received from server. Please wait while the database is being reset. This may take a few hours&#133;', '__hardreset');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                AddBootStrapDangerAlert("#videoUploadStatusPlaceholder", "Failed to hard reset database. Reason: " + errorThrown);
            }
        });
    }

    function getQueriesCSV() {
        var baseUrl = siteUrl + '/queries.csv';

        $.ajax({
            type : "GET",
            url : baseUrl,
            success : function(data) {
                console.log('Got queries csv');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert("get queries failed " + errorThrown);
            }
        });

    }

    /**
     * Open dialog to download queries as csv file for printing.
     */
    function getQueryCSVToPrint() {
        var baseUrl = siteUrl + "/queries/search";

        function getQueryObjectsAsArrays(queryData) {
            var queryObjectsAsArrays = new Array();

            for (var i = 0; i < queryData.length; i++) {
                var ts = queryData[i].timestamp;
                var q = "\""+queryData[i].query+"\"";
                var ip = queryData[i].ip_address;
                var hit = queryData[i].found;

                queryObjectsAsArrays.push([ts, q, ip, hit]);
            }

            return queryObjectsAsArrays;
        }

        $.ajax({
            type : "GET",
            url : baseUrl,
            success : function(queryDataObjects) { // create csv for download
                var queryDataArrays = getQueryObjectsAsArrays(queryDataObjects);
                var csvContent = "data:text/csv;charset=utf-8,";

                csvContent += 'Timestamp, Query, IP Address, Found\n';

                queryDataArrays.forEach(function(infoArray, index){
                    dataString = infoArray.join(",");
                    csvContent += dataString + "\n";
                });

                var encodedUri = encodeURI(csvContent);

                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth()+1;
                var yyyy = today.getFullYear();
                var date = "" + mm + dd + yyyy

                // set csv to download on link click (allows us to set file name)
                var link = document.createElement("a");
                link.setAttribute("href", encodedUri);
                link.setAttribute("download", "querydump_"+date+".csv");

                // force link click
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert("get session failed " + errorThrown);
            }
        }); //TODO add failure call back
    }

    /**
     * Open dialog to download videos as csv file for printing.
     */
    function getVideoCSVToPrint() {
        var baseUrl = siteUrl + "/listallvideos";

        function getQueryObjectsAsArrays(queryData) {
            var queryObjectsAsArrays = new Array();

            for (var i = 0; i < queryData.length; i++) {
                var id = queryData[i].id;
                var title = "\""+queryData[i].title+"\"";
                var keywords = "\""+queryData[i].keywords.join(',')+"\"";
                var related_words = "\""+queryData[i].related_words.join(',')+"\"";

                queryObjectsAsArrays.push([id, title, keywords, related_words]);
            }

            return queryObjectsAsArrays;
        }

        $.ajax({
            type : "GET",
            url : baseUrl,
            success : function(queryDataObjects) { // create csv for download
                var queryDataArrays = getQueryObjectsAsArrays(queryDataObjects);
                var csvContent = "data:text/csv;charset=utf-8,";

                csvContent += 'YouTube ID, Title, Keywords, Related Words, Thumbnail\n';

                queryDataArrays.forEach(function(infoArray, index){
                    dataString = infoArray.join(",");
                    csvContent += dataString + "\n";
                });

                var encodedUri = encodeURI(csvContent);

                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth()+1;
                var yyyy = today.getFullYear();
                var date = "" + mm + dd + yyyy

                // set csv to download on link click (allows us to set file name)
                var link = document.createElement("a");
                link.setAttribute("href", encodedUri);
                link.setAttribute("download", "videodump_"+date+".csv");

                // force link click
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert("get session failed " + errorThrown);
            }
        }); //TODO add failure call back
    }

    //TODO: consider moving the video upload apis to a different js file
	var TIMEOUT_30_SEC = 30000; //30 secs in ms
	var spinnerOptions = {
	  lines: 11, // The number of lines to draw
	  length: 0, // The length of each line
	  width: 6, // The line thickness
	  radius: 10, // The radius of the inner circle
	  corners: 1, // Corner roundness (0..1)
	  rotate: 0, // The rotation offset
	  direction: 1, // 1: clockwise, -1: counterclockwise
	  color: '#000', // #rgb or #rrggbb or array of colors
	  speed: 1, // Rounds per second
	  trail: 60, // Afterglow percentage
	  shadow: false, // Whether to render a shadow
	  hwaccel: false, // Whether to use hardware acceleration
	  className: 'spinner', // The CSS class to assign to the spinner
	  zIndex: 2e9, // The z-index (defaults to 2000000000)
	  top: '10', // Top position relative to parent
	  left: '10', // Left position relative to parent
	  position: 'relative'
	};

	function setAccessToken(token) {
		accessToken = token;
	}

	function submitVideoChanges(e) {
		e.preventDefault();
		clearBootStrapPlaceholder('#editVideoErrorPlaceholder');

		var vidId = $("#editID").val();
		var titleIsEmpty = $('#editVideoTitle').val() === "" || $('#editVideoTitle').val() === null;
		var descriptionIsEmpty = $('#editVideoDescription').val() === ""
			|| $('#editVideoDescription').val() === null;
		if(titleIsEmpty) {
			AddBootStrapDangerAlert('#editVideoErrorPlaceholder', 'You must supply a title');
			return;
		}

	  	$('#editUpload').attr('disabled', true);
	  	$('#editCancel').attr('disabled', true);
	  	$('#' + vidId).find(".edit-video-button").attr('disabled', true);

	  	var metadata = {
			id: vidId,
			snippet: {
				title: $('#editVideoTitle').val(),
				description: $('#editVideoDescription').val(),
				tags: $('#editVideoTags').val().split(','),
				//tags: $('#editVideoTags').val().split(' '),
				categoryId: 27 //TODO: might need to change this to look up categories and give
							   //the user a choice
			},
			status: {
				privacyStatus: $(".edit-privacy.active input").val(),
                embeddable: true
			}
	  	};

		var baseUrl = siteUrl +  "/videos/";
	  	var ajax = $.ajax({
		        timeout : TIMEOUT_30_SEC,
			url: baseUrl + vidId,
			method: 'PUT',
			dataType: 'json',
			contentType: 'application/json',
			data: JSON.stringify(metadata)
	  	});

	  	ajax.done(function(data, textStatus, jqXHR) {
	  		$('#editUpload').attr('disabled', false);
	  		$('#editCancel').attr('disabled', false);
	  		$('#' + vidId).find(".edit-video-button").attr('disabled', false);

	  		var row = $("#" + vidId);

	    	if(row) {
                var index = row.find(".edit-video-btn").attr('data-index');
                videoTableRows_[index].privacyStatus = data.privacyStatus;
				row.find(".tr-title").text(data.title);
				var tags = row.find(".tr-keywords");
				tags.empty();
				for (key in data.keywords) {
					tags.append($('<span>')
							.addClass('label label-default keyword')
							.text(data.keywords[key]));
				}
	    	}

	  		AddBootStrapSuccessAlert("#videoUploadStatusPlaceholder", "<strong>Success:</strong> "+data.title+" has been updated");
	  		$("#editVideoModal").modal("hide");
	  	});

	  	ajax.fail(function( jqXHR, textStatus, errorThrown) {
	  		$('#editUpload').attr('disabled', false);
	  		$('#editCancel').attr('disabled', false);
	  		$('#' + vidId).find(".edit-video-button").attr('disabled', false);

	   		AddBootStrapDangerAlert("#editVideoErrorPlaceholder", "Failed to update video. Reason: " + errorThrown);
	  	});
	}

	function initVideoUpload(e) {
		e.preventDefault();
		clearBootStrapPlaceholder('#videoErrorPlaceholder');

		var file = $('#videoFile').get(0).files[0];
		var titleIsEmpty = $('#videoTitle').val() === "" || $('#videoTitle').val() === null;
		var descriptionIsEmpty = $('#videoDescription').val() === ""
			|| $('#videoDescription').val() === null;
		var doesNotHaveMissingData = true;
		if(titleIsEmpty) {
			doesNotHaveMissingData = false;
			AddBootStrapDangerAlert('#videoErrorPlaceholder', 'You must supply a title');
		}
		if (!file) {
			doesNotHaveMissingData = false;
			AddBootStrapDangerAlert('#videoErrorPlaceholder', 'You must choose a file');
		}

		if (doesNotHaveMissingData) {
		  	$('#upload').attr('disabled', true);
		  	$('#cancel').attr('disabled', true);

		  	var metadata = {
				snippet: {
					title: $('#videoTitle').val(),
					description: $('#videoDescription').val(),
					tags: $('#videoTags').val().split(','),
					categoryId: 27 //TODO: might need to change this to look up categories and give
								   //the user a choice
				},
				status: {
					privacyStatus: $(".privacy.active input").val(),
                    embeddable: true
				}
		  	};

            console.log('Uploading video with metadata = ' + JSON.stringify(metadata));

		  	var ajax = $.ajax({
	 	        timeout : TIMEOUT_30_SEC,
				url: 'https://www.googleapis.com/upload/youtube/v3/videos?uploadType=resumable&part=snippet,status',
				method: 'POST',
				contentType: 'application/json',
				headers: {
					Authorization: 'Bearer ' + accessToken,
					'x-upload-content-length': file.size,
					'x-upload-content-type': file.type
				},
				data: JSON.stringify(metadata)
		  	});

		  	ajax.done(function(data, textStatus, jqXHR) {
				UploadVideo({
					url: jqXHR.getResponseHeader('Location'),
					file: file,
					start: 0
				});
		  	});

		  	ajax.fail(function( jqXHR, textStatus, errorThrown) {
		  		alert('Something went wrong while uploading :(')
		  		console.log(jqXHR.responseText);
		  	});
		}
	}

	function UploadVideo(options) {
		var ajax = $.ajax({
	      timeout : TIMEOUT_30_SEC,
		  url: options.url,
		  method: 'PUT',
		  contentType: options.file.type,
		  headers: {
			'Content-Range': 'bytes ' + options.start + '-' + (options.file.size - 1) + '/' +
				options.file.size
		  },
		  xhr: function() {
			var xhr = $.ajaxSettings.xhr();

			if (xhr.upload) {
			  xhr.upload.addEventListener(
				'progress',
				function(e) {
				  if(e.lengthComputable) {
					var percentage = Math.round(100 * e.loaded / e.total);

					$('#uploadProgress').show();
					$('#uploadProgressBar').attr('data-transitiongoal', percentage);
					$('.progress .progress-bar').progressbar({display_text: 'fill'});
				  }
				},
				false
			  );
			}

			return xhr;
		  },
		  processData: false,
		  data: options.file
		});

		ajax.done(function(response) {
		  var videoID = response.id;
		  $('#uploadProgress').hide();
		  AddBootStrapInfoAlertWithSpinner('#videoUploadStatusPlaceholder',
			  'Video successfully uploaded! Checking video status. Please do not close the page&#133;', videoID);
		  $('#addVideoModal').modal('hide');
		  $('#uploadForm').find("input[type=text], textarea").val("");

	  	  $('#upload').attr('disabled', false);
		  $('#cancel').attr('disabled', false);
		  checkVideoStatus(videoID, 3000);
		});

		ajax.fail(function() {
		  $('#upload').click(function() {
			alert('Upload failed. Please try again later');
		  });


	  	  $('#upload').attr('disabled', false);
		  $('#cancel').attr('disabled', false);
		  //$('#upload').val('Resume Upload');
		  //$('#upload').attr('disabled', false);
		});
	}

	function checkVideoStatus(videoID, waitTime) {
	    var ajax = $.ajax({
	      timeout : TIMEOUT_30_SEC,
	      url: "https://www.googleapis.com/youtube/v3/videos",
	      method: 'GET',
	      headers: {
	        Authorization: 'Bearer ' + accessToken
	      },
	      data: {
	        part: 'status,processingDetails',
	        id: videoID
	      }
	    });

	    ajax.done(function(response) {
	      var processingStatus = response.items[0].processingDetails.processingStatus;

	      if (processingStatus == 'processing') {
	        setTimeout(function() {
	          checkVideoStatus(videoID, Math.min(waitTime * 1.5, 10000));
	        }, waitTime);
	      } else {
	        $("#processingAlert" + videoID).remove();
	      	var uploadStatus = response.items[0].status.uploadStatus;
	        if (uploadStatus == 'processed') {
	          $('#player').append(response.items[0]);
	          UpdateServerVideoCollection(videoID);
	          AddBootStrapInfoAlertWithSpinner("#videoUploadStatusPlaceholder",
	          	"<strong>Success:</strong> Video successfully uploaded and accepted by Youtube! Updating database&#133;", videoID);
	        } else if (uploadStatus == 'rejected'){
	        	AddBootStrapDangerAlert("#videoUploadStatusPlaceholder", "Youtube rejected the video. The reason was: " + response.items[0].status.rejectionReason);
	        }
	      }
	    });

	    ajax.fail(function(jhr, textStatus) {
	        $("#processingAlert" + videoID).remove();
	        AddBootStrapDangerAlert("#videoUploadStatusPlaceholder", "Unable to access video status. Reason: " + textStatus);
	    });
	  }

	function UpdateServerVideoCollection(videoID) {
		var baseUrl = siteUrl + "/videos/"

		$.ajax({
	      	timeout : TIMEOUT_30_SEC,
			type : "POST",
			url : baseUrl + videoID,
			success : function(result) {
	        	$("#processingAlert" + videoID).remove();
				AddBootStrapSuccessAlert("#videoUploadStatusPlaceholder", "<strong>Success:</strong> YouTube and Database have been updated!");
			},
			error: function (jqXHR, textStatus, errorThrown) {
	        	$("#processingAlert" + videoID).remove();
	       		AddBootStrapDangerAlert("#videoUploadStatusPlaceholder", "Failed to update Database. Reason: " + errorThrown);
			}
		});
	}

	function deleteVideo(videoID, button) {
		var baseUrl = siteUrl + "/videos/"

	    AddBootStrapInfoAlertWithSpinner("#videoUploadStatusPlaceholder",
	        "<strong>Success:</strong> Deleting Video with id" + videoID + "&#133;", videoID);

		$.ajax({
	      	timeout : TIMEOUT_30_SEC,
			type : "DELETE",
			url : baseUrl + videoID,
			success : function(result) {
	        	$("#processingAlert" + videoID).remove();
	        	var row = $("#" + videoID);
	        	if(row) {
	        		row.remove();
	        	}
				AddBootStrapSuccessAlert("#videoUploadStatusPlaceholder", "<strong>Success:</strong> YouTube and the Database removed video with id " + videoID);
			},
			error: function (jqXHR, textStatus, errorThrown) {
	        	$("#processingAlert" + videoID).remove();
	        	button.attr('disabled', false);
	       		AddBootStrapDangerAlert("#videoUploadStatusPlaceholder", "Failed to delete video. Reason: " + errorThrown);
			}
		});
	}

	function AddBootStrapDangerAlert(alertPlaceHolder, message) {
		AddBootStrapAlert(alertPlaceHolder, 'alert alert-danger alert-error',
				'<strong>Error: </strong>' + message, false, null);
	}

	function AddBootStrapInfoAlert(alertPlaceHolder, message) {
		AddBootStrapAlert(alertPlaceHolder, 'alert alert-info', message, false, null);
	}

	function AddBootStrapSuccessAlert(alertPlaceHolder, message) {
		AddBootStrapAlert(alertPlaceHolder, 'alert alert-success', message, false, null);
	}

	function AddBootStrapInfoAlertWithSpinner(alertPlaceHolder, message, id) {
		AddBootStrapAlert(alertPlaceHolder, 'alert alert-info', message, true, id);
	}

	function AddBootStrapAlert(alertPlaceHolder, alertClass, message, messageHasSpinner, id) {
		if(messageHasSpinner === true) {
			var spinner = new Spinner(spinnerOptions).spin();
			$(alertPlaceHolder).append('<div class="'+ alertClass +
				'" role="alert" id="processingAlert' + id + '"><span id="spinnerPH' + id +'"></span><span class="alert-with-spinner">'+ message +'</span></div>');
			$("#spinnerPH"+ id).append(spinner.el);
		} else {
			$(alertPlaceHolder).append('<div class="'+ alertClass +
				'" role="alert"><a class="close" data-dismiss="alert">×</a><span>'+ message +'</span></div>');
		}
	}

	function clearBootStrapPlaceholder(placeholder) {
		$(placeholder).html('');
	}
}