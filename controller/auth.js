var googleapis = require('googleapis');
var OAuth2 = googleapis.auth.OAuth2;

var client = '232391267423.apps.googleusercontent.com'; //SCOTT: This should be changed to the clientId of your google dev account
var secret = 'y4ok-lnMHtWqLrHvIHh7MZCt'; //SCOTT: This should be changed to the client secret of your dev account
var redirect = process.env.OPENSHIFT_APP_DNS ? 'https://' + process.env.OPENSHIFT_APP_DNS + '/admin'
    : 'http://localhost:8083/admin'; //SCOTT: This needs to be changed to the {domain name of the website}/admin

console.log('redirect = ' + redirect);

var oauth2Client = new OAuth2(client, secret, redirect);

var youtube_auth_url = oauth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: 'https://www.googleapis.com/auth/youtube https://www.googleapis.com/auth/youtube.upload https://www.googleapis.com/auth/youtube.readonly https://www.googleapis.com/auth/userinfo.email'
});

var oauth2 = googleapis.oauth2('v2');
var plus = googleapis.plus('v1');

exports.oauth = oauth2;
exports.client = oauth2Client;
exports.url = youtube_auth_url;
exports.plus = plus;

//SCOTT: This list of emails should be changed to the email address of the accounts that Harley wishes to use
exports.validUsers = [
    'thedictionary13@gmail.com',
    'smartsigndictionary@gmail.com'
    // 'zhangksteven@gmail.com',
    // 'meyer.justian@gmail.com',
    // 'test-channel-8630@pages.plusgoogle.com',
    // 'grab.turkey@gmail.com',
    // 'meyer.justian@gmail.com'
    ];

exports.getToken = function(code, callback) {
    console.log('auth.setToken ', code);
    oauth2Client.getToken(code, function(err, token) {

        if (!err) {
            oauth2Client.setCredentials(token);
        }

        callback(err, token);

        //oauth2Client.credentials = token;
        // if(!oauth2Client.credentials || oauth2Client.credentials.error_description || isEmpty(oauth2Client.credentials)) {
        //     oauth2Client.credentials = null;
        //     redirect(true);
        // } else {
        //     redirect(false);
        // }
    });
};

exports.changeUser = function(req, res) {
    // oauth2Client.credentials = null;
    exports.revokeCredentials(req, res, function (error) {
       // if (error) {
       //     res.send('Error attempting to change user');
       // }
       // else {
           res.redirect(youtube_auth_url);
       // }
    });
}

exports.revokeCredentials = function(req, res, next) {
    if (oauth2Client) {
        oauth2Client.revokeCredentials(function (error, result) {
            if (error) {
                console.error('Failed to revoke credentials!');
                console.error(error || result);
            }
            req.session.reset();
            next();
        });
    }
}

exports.requireAuthorization = function (req, res, next) {
    // if we are coming back from Google's oauth redirect
    // console.log('requireAuthorization: code = ' + req.query.code);
    console.log('requireAuthorization');

    if (req.query.code) {
        console.log('Returning from Google oauth2 redirect');
        oauth2Client.getToken(req.query.code, function(err, tokens) {
           if (!err) {
               oauth2Client.setCredentials(tokens);
               req.session.credentials = tokens;
               next();
           }
           else {
               console.log('Error getting OAuth2 tokens: ', err);
               exports.revokeCredentials(req, res, function(error) {
                   res.redirect(youtube_auth_url);
               });
           }
        });
    }
    // if we haven't authenticated with Google yet, redirect to oauth url
    else if (!req.session.credentials) {
        console.log('Not authorized, redirecting');
        res.redirect(youtube_auth_url);
    }
    // otherwise, set oauth credentials from saved session tokens
    else {
        console.log('setCredentials from session.');
        oauth2Client.setCredentials(req.session.credentials);
        next();

        // get user info
        // exports.getUserName(function(error) {
        //     if (error) {
        //         exports.revokeCredentials(req, res, function(error) {
        //             res.redirect(youtube_auth_url);
        //         });
        //     }
        //     else {
        //         next();
        //     }
        // })
    }
    // next();
}


// exports.isAuthorized = function() {
//     if(!oauth2Client || !oauth2Client.credentials || isEmpty(oauth2Client.credentials)) {
//         //console.log("user was missing credentials");
//         //console.log('isAuthorized Error: oauth2Client.credentials = ', oauth2Client.credentials);
//         return false;
//     }
//     else {
//         return true;
//     }
// }

exports.getUserName = function(req, res, next) {
    console.log('auth.getUserName: Getting User Info');

    if (!req.session.currentUser) {
        oauth2.userinfo.get({
                auth: oauth2Client
            },
            function (err, results) {
                if (err) {
                    console.log('Error getting userinfo: ', err);
                    //callback(err);
                }
                else if (!results.email) {
                    console.log("results.email is empty from oauth user info request");
                    // callback(true);
                }

                console.log(results.email + " is requesting access");
                if (exports.validUsers.indexOf(results.email) > -1) {
                    req.session.currentUser = results.name;
                    // exports.currentUser = results.name;
                    // callback(err);
                }
                next();
                // } else {
                //     callback(true);
                // }
            }
        );
    }
    else {
        next();
    }
};

function isEmpty(map) {
    for(var key in map) {
        if (map.hasOwnProperty(key)) {
            return false;
        }
    }
    return true;
};
