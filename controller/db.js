var mongoose = require('mongoose');

// Use native promises
mongoose.Promise = global.Promise;

// default to a 'localhost' configuration:
// var connection_string = 'smartsign:F1anB7Rh0B577FkO@127.0.0.1:65006/smartsign';
var connection_string = '127.0.0.1:27017/smartsign';

// if OPENSHIFT env variables are present, use the available connection info:
if(process.env.OPENSHIFT_MONGODB_DB_PASSWORD) {
    // connection_string = process.env.OPENSHIFT_MONGODB_DB_URL + 'smartsign';

    connection_string = "mongodb://smartsign:F1anB7Rh0B577FkO@" +
        process.env.OPENSHIFT_MONGODB_DB_HOST + ':' +
        process.env.OPENSHIFT_MONGODB_DB_PORT + '/' +
        'smartsign';

    // connection_string = process.env.OPENSHIFT_MONGODB_DB_USERNAME + ":" +
    //     process.env.OPENSHIFT_MONGODB_DB_PASSWORD + "@" +
    //     process.env.OPENSHIFT_MONGODB_DB_HOST + ':' +
    //     process.env.OPENSHIFT_MONGODB_DB_PORT + '/' +
    //     process.env.OPENSHIFT_APP_NAME;
}

// var dbhost  = 'localhost'; //process.env.OPENSHIFT_NOSQL_DB_HOST;
// var dbport  = '27017'; //process.env.OPENSHIFT_NOSQL_DB_PORT;
// var dbuname = 'smartsign'; //process.env.OPENSHIFT_NOSQL_DB_USERNAME;
// var dbpwd   = '!carrot&1STICK';

// Establish connection to MongoDB
// var connection_string = 'mongodb://'+dbhost+':'+dbport+'/smartsign';
console.log('connection_string is ' + connection_string);

mongoose.connect(connection_string);

// mongoose.connect('mongodb://'+connection_string);

mongoose.connection.on('open', function() {
    console.log('db: Connected to MongoDB with mongoose!');
});

mongoose.connection.on('error', function() {
    console.log('db: Error connecting to MongoDB with mongoose!');
});
