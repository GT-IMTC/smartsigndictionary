//var app = require('../simple-youtube-sync.js');
var request = require('request');
var util = require('util');
var async = require('async');
var google_api = require('./gapi.js');

var numSyncedVideos = 0;

//var auth = require('./auth.js');
//var googleapis = require('googleapis');
var Video = require('../model/video.js');

//var oauth2Client = app.oauth2Client;

//var youtube = googleapis.youtube('v3');

// googleapis v1.0
//var youtube = googleapis.youtube({ version: 'v3', auth: auth.client });
var youtube = google_api.youtube;

exports.syncVideos = function(req, res, cb) {
	//console.log('syncVideos');
	var uploadsPlaylistId = 'UUACxqsL_FA-gMD2fwil7ZXA';
	var numVideos, maxResults = 2, numPages;
	var nextPageToken = '';
	var done = false;
	var retryLimit = 5; // number of times to retry a failed Youtube API call

    numSyncedVideos = 0;

	function printResult(err, result) {
		if (err) {
		  //console.log('printResult: Error occurred: ', err);
		} else {
		  //console.log('Video: ', util.inspect(result, {depth:null}));
		  nextPageToken = result.nextPageToken;
		}
	};

	var saveVideoEntry = function (video, callback) {
        // if (err || video === undefined) {
        if (video === undefined) {
			console.error('*** Error occurred in saveVideoEntry, video = ', video);
			callback('Error in saveVideoEntry');
		}

		var localVideo = {};
		var relatedWords = [];//, keywords = [];
		//	var description;
		var keyRelatedWordsJSON, keyRelatedWords;
		var vidSnippet;
		var videoId;

		try {
			vidSnippet = video.snippet;
			videoId = video.id;
		}
		catch (e) {
			console.error('Error: saveVideoEntry video.items = ' + util.inspect(video, {depth:null}));
		}

		if (!vidSnippet) {
		    //console.log('Error vidSnippet is null' + util.inspect(video, {depth:null}));
        }

		////console.log('saveVideoEntry:');
		//printResult(err, video);

		try {
			keyRelatedWordsJSON = vidSnippet.description.slice(vidSnippet.description.indexOf("{"), vidSnippet.description.lastIndexOf("}") + 1);
		}
		catch (e) {
			console.error('Error parsing keyRelatedWords for ' + videoId + ' : ' + e.message);
			keyRelatedWordsJSON = '';
		}
		if (keyRelatedWordsJSON && keyRelatedWordsJSON != '') {
			try {
				keyRelatedWords = JSON.parse(keyRelatedWordsJSON);
				relatedWords = keyRelatedWords.related_words || [];
				//keywords = keyRelatedWords.keywords || [];
			}
			catch (e) {
				//console.log('Trying to parse invalid JSON: ' + relatedWords);
			}
		}

		// //console.log('id = ' + videoId + ' vidSnippet = ' + util.inspect(vidSnippet, {depth:null}));

		// TODO add the ID of the user as enumeration... store the enumeration
		// in gapi.js
		localVideo = {
			id : videoId,
			title : vidSnippet.title || '',
			description : vidSnippet.description || '',
			thumbnail : vidSnippet.thumbnails.default.url,
			thumbnailMedium : vidSnippet.thumbnails.medium ? vidSnippet.thumbnails.medium.url : '',
			thumbnailHigh : vidSnippet.thumbnails.high ? vidSnippet.thumbnails.high.url : '',
			thumbnailStandard : vidSnippet.thumbnails.standard ? vidSnippet.thumbnails.standard.url : '',
			keywords : (vidSnippet.tags) ? vidSnippet.tags.map(function(tag) { return tag.toLowerCase(); }) : [],
			related_words : relatedWords
		};
		////console.log('Saving a video (' + video.id + '): ' + video.title);
		//printResult(err, video);

		// First, try to find existing video record to update
		Video.findOne({ id: localVideo.id }, function(err, vid) {
			////console.log('Video.findOne id = ' + video.id);
			if (err) {
				//return handleError(err);
				//console.log('Video.fineOne: Error = ', err);
				//throw err;
                callback(err);
			}
			if (vid) {
				////console.log('Found video ' + vid.title + '!!!');
				vid.title = localVideo.title;
				vid.description = localVideo.description;
				vid.keywords = localVideo.keywords;
				vid.related_words = localVideo.related_words;
				vid.thumbnail = localVideo.thumbnail;
				vid.thumbnailMedium = localVideo.thumbnailMedium;
				vid.thumbnailHigh = localVideo.thumbnailHigh;
				vid.thumbnailStandard = localVideo.thumbnailStandard;
			}
			else {
				////console.log('!!!! Creating new Video for ' + video.title);
				vid = new Video(localVideo);
			}
			vid.save(function (err) {
				if (err) {
				    //throw err;
                    callback(err);
                }
				//console.log('Video ' + vid.title + ' saved.');
                numSyncedVideos++;
                callback(err);
				////console.log(vid);
			});
		});
	};

	//function saveVideoEntry(video) {
	//	//console.log('Saving video ' + video.id);
    //
	//	// First, try to find existing video record to update
	//	Video.findOne({ id: video.id }, function(err, vid) {
	//		////console.log('Video.findOne id = ' + video.id);
	//		if (err) return handleError(err);
	//		if (vid) {
	//			////console.log('Found video ' + vid + '!!!');
	//			vid.title = video.title;
	//			vid.description = video.description;
	//			vid.keywords = video.keywords;
	//			vid.related_words = video.related_words;
	//			vid.thumbnail = video.thumbnail;
	//		}
	//		else {
	//			//console.log('!!!! Creating new Video');
	//			vid = new Video(video);
	//		}
	//		vid.save(function (err) {
	//			if (err) throw err;
	//			//console.log('Video ' + vid.id + ' saved.');
	//			////console.log(vid);
	//		});
	//	});
	//};

	var queue = async.queue(function(playlistItems, callback) {
		saveVideoEntries(playlistItems, function(err) {
		    callback(err);
        });
		// callback();
	}, 10);

	queue.drain = function() {
	    //console.log('Saved ' + numSyncedVideos + ' videos');
		checkDone();
	};

	function getPlaylistItems(nextPageToken) {
		//console.log('getPlaylistItems.nextPageToken = ' + nextPageToken);

		// get next page of videoIds, then request details for each videoId
		youtube.playlistItems.list(
			{
				playlistId: uploadsPlaylistId,
				maxResults: 50,
				pageToken: nextPageToken,
				part: 'snippet',
				fields: 'items(snippet(resourceId/videoId)),nextPageToken'
			},
			function(err, playlistItems) {
				if (err) {
					//console.log('getPlaylistItems: Error occurred: ', err);
					//console.log('getPlaylistItems: Trying to retrieve nextPageToken = ' + nextPageToken);
					retryLimit--;
				} else {
					nextPageToken = playlistItems.nextPageToken;
					////console.log('nextPageToken = ' + nextPageToken);
					queue.push(playlistItems, function(err) {
						if (err) {
							//console.log('*** queue error: ', err);
						}
						//console.log('Retrieved playlistitems for pageToken ' + nextPageToken);
					});
				}

				if (nextPageToken && retryLimit > 0) {
					getPlaylistItems(nextPageToken);
				}
				else if (retryLimit > 0) {
					////console.log('getPlaylistItems: Done!!!');
					done = true;
				}
				else {
					//console.log('Error: Too many errors encountered with youtube.playlistItems.list!!!');
					done = true;
				}
			});
	}

	//function getPlaylistItems(nextPageToken) {
	//	//console.log('getPlaylistItems.nextPageToken = ' + nextPageToken);
    //
	//	youtube.playlistItems.list({
	//		playlistId: uploadsPlaylistId,
	//		maxResults: 4,
	//		pageToken: nextPageToken,
	//		part: 'snippet',
	//		fields: 'items(snippet(resourceId/videoId,thumbnails/default,title,description)),nextPageToken'
	//	}, function(err, result) {
	//		if (err) {
	//			//console.log('getPlaylistItems: Error occurred: ', err);
	//			//console.log('getPlaylistItems: Error occurred: ', result);
	//		} else {
	//			nextPageToken = result.nextPageToken;
	//			queue.push(result, function(err) {
	//				//console.log('nextPageToken = ' + nextPageToken);
	//			});
    //
	//			if (nextPageToken) {
	//				getPlaylistItems(nextPageToken);
	//			}
	//			else {
	//				//console.log('*********** Done updating video library **************');
	//				done = true;
	//			}
	//		}
	//	});
	//}

	//function getVideoDetails(vid) {
	//	//console.log('Getting details for video ' + vid);
    //
	//	//client.youtube.
	//}

	function getVideoDetails(videoId, callback) {
		youtube.videos.list({
            part: 'snippet',
            // part: 'id,snippet',
			id: videoId,
			fields: 'items(id,snippet(description,tags,thumbnails,title))'
		}, callback);
	}

	function checkDone() {
		if (queue.length() == 0 && done) {
			////console.log('Finished listing all Video objects!');
			cb();
		}

	}

	function saveVideoEntries(playlistItems, callback) {
        var videoIds;
        // for each playlist item, get video details and saveVideoEntry on playlistItems

        videoIds = playlistItems.items.map(function(item) { return item.snippet.resourceId.videoId; }).join(',');

        getVideoDetails(videoIds, function (err, videos) {
            //console.log('Getting details for ' + videos.items.length + ' videos...');
            async.eachLimit(videos.items, 10, function(video, cb) {
                saveVideoEntry(video, function(err) {
                    //console.log('Done saving video');
                    if (err) {
                        //console.log('Error: ' + err);
                    }

                    cb(err);
                });
            }, function(err) {
                //console.log('Done getting videos details!!! err = ' + err);

                callback(err);
            });
        });

        // async.eachSeries(playlistItems.items, function (item) {
        //     videoId = item.snippet.resourceId.videoId;
        //
        //     getVideoDetails(videoId, function (err, videos) {
        //         saveVideoEntry(videos, function(err) {
        //             //console.log('Done saving video');
        //             callback(err);
        //         });
        //     });
        // }), function (err) {
        //     callback(err);
        //     // playlistItems.items.forEach(function(item) {
        //     // 	videoId = item.snippet.resourceId.videoId;
        //     //
        //     // 	getVideoDetails(videoId, saveVideoEntry);
        //     //
        //     // 	//client.videos.list({
        //     // 	//	part: 'id,snippet',
        //     // 	//	id: videoId,
        //     // 	//	auth: authClient
        //     // 	//	//fields: 'items(id,snippet)'
        //     // 	//}, saveVideoEntry);
        //     // });
        // }
    }

	//function saveVideoEntries(result) {
	//	var video = {};
	//	var relatedWords = [], keywords = [];
	//	var description;
	//	var keyRelatedWordsJSON, keyRelatedWords;
    //
	//	result.items.forEach(function (item) {
	//		var description = item.snippet.description;
	//		var keyRelatedWordsJSON = description.slice(description.indexOf("{"), description.lastIndexOf("}") + 1);
	//		var relatedWords = [];
	//		var keywords = [];
	//		//video = {};
    //
	//		if (keyRelatedWordsJSON && keyRelatedWordsJSON != '') {
	//			try {
	//				keyRelatedWords = JSON.parse(keyRelatedWordsJSON);
	//				relatedWords = keyRelatedWords.related_words || [];
	//				keywords = keyRelatedWords.keywords || [];
	//			}
	//			catch (e) {
	//				//console.log('saveVideoEntries: Trying to parse invalid JSON: ' + relatedWords);
	//			}
	//		}
    //
	//		var video = {
	//			id : item.snippet.resourceId.videoId,
	//			title : item.snippet.title,
	//			description : item.snippet.description,
	//			thumbnail : item.snippet.thumbnails.default.url,
	//			keywords: keywords,
	//			related_words : relatedWords
	//		};
	//		//printResult(null, video);
	//		saveVideoEntry(video);
	//	});
	//}

	getPlaylistItems(null);
};