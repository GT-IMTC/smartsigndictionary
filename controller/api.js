var Query = require('../model/query.js');
var Video = require('../model/video.js');
var async = require('async');
var youtube_sync = require('./api-youtube-sync.js');

// var fs = require('fs');

exports.saveQuery = function(req, res) {
    var query = new Query({ query: req.body.query, found: req.body.found,
    	ip_address: getClientIp(req), user: req.body.user, study: req.body.study });

    query.save(function (err) {
        if (err) throw err;
        //console.log('Query saved. Client ip is ' + getClientIp(req));

        res.send('{"records":' +  JSON.stringify(query) + '}');
    });
};

exports.updateQuery = function(req, res) {
    var query = new Query({ _id: req.body._id, query: req.body.query, found: req.body.found,
    	ip_address: req.body.ip_address, user: req.body.user, study: req.body.study });

    query.save(function (err) {
        if (err) throw err;
        // console.log('Query saved.');

        res.send('Query saved.');
    });
};

exports.listQueries = function(req, res) {
    var queryOptions = { sort: { timestamp: -1 }},
        skip = req.query.skip,
        limit = req.query.limit;

    if (skip) {
        queryOptions.skip = parseInt(skip);
    }

    if (limit) {
        queryOptions.limit = parseInt(limit);
    }

     Query.count().then(function (count) {
        Query.find(null, null, queryOptions, function(err, queries) {
            var result = {
                success: true,
                total: count,
                data: queries
            }
             res.jsonp(result);
        });
    });
};

exports.showQuery = function(req, res) {
    //Query.findOne({ _id: req.params.id }, function(error, query) {
    //console.log('finding ' + req.params._id);
    Query.findById(req.params._id, function(error, query) {
        //res.send([{ Query: query }]);
        res.jsonp(query);
    });
}

exports.listQueriesDateRange = function(req, res) {
    var startDate = req.query.start,
        endDate = req.query.end,
        start = startDate ? Date.parse(startDate) : null,
        end = endDate ? Date.parse(endDate) : null,
        rangeParams = (start || end) ? { timestamp: {} } : {};

    if (start) {
        rangeParams.timestamp.$gte = start;
    }

    if (end) {
        rangeParams.timestamp.$lt = end;
    }

    var startTime = start || 'all';
    res.attachment('queries_' + startTime + '.csv');
    res.write('query, found, ip_address, timestamp\n');

    Query.find().cursor(rangeParams)
        .on('data', function(query) {
             var str = '';

             str += '"' + query.query + '"';
             str += ', ' + query.found;
             str += ', ' + query.ip_address;
             str += ', ' + query.timestamp;
             str += '\n';

             // console.log(str);
             res.write(str);
        })
        .on('end', function() {
             // console.log('Done streaming queries.csv');
             res.end();
        })
        .on('error', function(err) {
            console.log('Error while streaming queries.csv: ' + err.message);
        });
}


// function returnQueries(req, res, start) {
//     start = start || 'all';
//
//     // send as CSV file
//     if (req.params.format) {
//         return function(error, queries) {
//             res.attachment('queries_' + start + '.csv');
//             res.send(queriesToCsv(queries));
//         }
//     }
//     else {
//         return function(error, queries) {
//             res.jsonp(queries);
//         }
//     }
// }
//
// function queryToCsv(query) {
//     var str = '';
//
//     // query.forEach(function (query) {
//         str += '"' + query.query + '"';
//         str += ', ' + query.found;
//         str += ', ' + query.ip_address;
//         str += ', ' + query.timestamp;
//         str += '\n';
//     // });
//     // return str;
// }

// function queriesToCsv(queries) {
//     var str = 'query, found, ip_address, timestamp\n';
//
//     queries.forEach(function (query) {
//         str += '"' + query.query + '"';
//         str += ', ' + query.found;
//         str += ', ' + query.ip_address;
//         str += ', ' + query.timestamp;
//         str += '\n';
//     });
//     //res.header('Content-type', 'text/csv');
//     //res.send(str);
//     return str;
// }

/**
 * search for queries based on search terms
 */
exports.searchQueries = function(req, res) {
	var keyword = req.params.keyword ? req.params.keyword : "";
	var searchQuery = keyword ? {query : keyword.toLowerCase()} : {};

    var queryOptions = { sort: { timestamp: -1 }},
        skip = req.query.skip,
        limit = req.query.limit;

    if (skip) {
        queryOptions.skip = parseInt(skip);
    }

    if (limit) {
        queryOptions.limit = parseInt(limit);
    }

    Query.count(searchQuery, function (err, count) {
        Query.find(searchQuery, null, queryOptions, function (err, queries) {
            var result = {
                success: true,
                total: count,
                data: queries
            }
            res.jsonp(result);
        });
    });
}

/**
 * searches for the video with given title
 */
exports.searchVideos = function(req, res) {
    var keyword = req.params.keyword ? req.params.keyword : "";
    var titleQuery = keyword ? {keywords: keyword.toLowerCase()} : {};

    var queryOptions = { sort: { title: 1 }},
        skip = req.query.skip,
        limit = req.query.limit;

    if (skip) {
        queryOptions.skip = parseInt(skip);
    }

    if (limit) {
        queryOptions.limit = parseInt(limit);
    }

    Video.count(titleQuery, function (err, count) {
        Video.find(titleQuery, null, queryOptions, function (err, videos) {
            var result = {
                success: true,
                total: count,
                data: videos
            }
            res.jsonp(result);
        });
    });
}


// exports.listVideos = function(req, res) {
// 	console.log("listVideos");
// 	Video.find("", function(err, videos) {
// 		//res.setHeader('Content-Type', 'text/javascript;charset=UTF-8');
//         //res.send('{"records":' +  JSON.stringify(videos) + '}');
// 		res.jsonp(videos);
// 	});
// };

exports.listVideos = function(req, res) {
    var queryOptions = { sort: { title: 1 }},
        condition = {},
        skip = req.query.skip,
        limit = req.query.limit;

    if (skip) {
        queryOptions.skip = parseInt(skip);
    }

    if (limit) {
        queryOptions.limit = parseInt(limit);
    }

    var keywords = req.query.keywords;

    if (keywords) {
        condition.keywords = keywords.toLowerCase();

        // Video.find({ 'keywords': keywords.toLowerCase() }, function(error, videos) {
        //     //res.setHeader('Content-Type', 'text/javascript;charset=UTF-8');
        //     res.jsonp(videos);
        // });
    }

    Video.count(condition).then(function (count) {
        Video.find(condition, null, queryOptions, function(err, videos) {
            var result = {
                success: true,
                total: count,
                data: videos
            }
            res.jsonp(result);
        });
    });
};

exports.showVideoById = function(req, res) {
	// console.log('showVideoById ' + req.params.id);
    Video.findOne({ id: req.params.id}, function(error, video) {
		//res.setHeader('Content-Type', 'text/javascript;charset=UTF-8');
        res.jsonp(video);
    });
}

exports.insertVideoById = function(req, res) {
	// console.log('insertVideoById ' + req.params.id);
    res.send('Video saved.');
}

exports.deleteVideoById = function(req, res) {
	// console.log('deleteVideoById ' + req.params.id);
	
    res.send('Video deleted.');
}

exports.hardResetVideos = function(req, res) {
    //deleteAllVideos();
    // console.log('hardResetVideos');
    youtube_sync.syncVideos(req, res, function() {
        console.log('Done with hardResetVideos');
    });
    res.send('Videos successfully reset.');
    //TODO do actual success callback (not working cuz async)
}

function deleteAllVideos() {
    Video.remove({}, function() {
        console.log('deleteAllVideos');
    });
}

exports.findVideosByKeyword = function(req, res) {
	//console.log('showVideoByKeyword ' + util.inspect(req.query));
	var keywords = req.query["keywords"];

	if (keywords) {
	    Video.find({ 'keywords': keywords.toLowerCase() }, function(error, videos) {
			//res.setHeader('Content-Type', 'text/javascript;charset=UTF-8');
	        res.jsonp(videos);
	    });
	}
	else {
		Video.find(function(err, videos) {
			//res.setHeader('Content-Type', 'text/javascript;charset=UTF-8');
	        //res.send('{"records":' +  JSON.stringify(videos) + '}');
	        res.jsonp(videos);
		});
	}
}

exports.listAllVideos = function(req, res) {
    // Video.find({}, '-_id id title keywords related_words', function(err, videos) {
    //     res.jsonp(videos);
    // });
    
    // Video.find(returnVideos(req, res));

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1;
    var yyyy = today.getFullYear();
    var date = "" + mm + dd + yyyy

    res.attachment('videos_' + date + '.csv');
    res.write('YouTube ID, Title, Keywords, Related Words, Thumbnail, Privacy Status\n');

    Video.find().cursor()
        .on('data', function(video) {
            var str = '';

            str += '"' + video.id + '"';
            str += ',"' + video.title + '"';
            str += ',"' + video.keywords + '"';
            str += ',"' + video.related_words + '"';
            str += ', ' + video.thumbnail;
            str += ', ' + video.privacyStatus;
            str += '\n';

            // console.log(str);
            res.write(str);
        })
        .on('end', function() {
            console.log('Done streaming videos.csv');
            res.end();
        })
        .on('error', function(err) {
            console.log('Error while streaming videos.csv: ' + err.message);
        });
}

// function returnVideos(req, res) {
//     var today = new Date();
//     var dd = today.getDate();
//     var mm = today.getMonth()+1;
//     var yyyy = today.getFullYear();
//     var date = "" + mm + dd + yyyy
//
//     // send as CSV file
//     if (req.params.format) {
//         return function(error, videos) {
//             res.attachment('videos_' + date + '.csv');
//             res.send(videosToCsv(videos));
//         }
//     }
//     else {
//         return function(error, videos) {
//             res.jsonp(videos);
//         }
//     }
// }

// function videosToCsv(videos) {
//     var str = 'YouTube ID, Title, Keywords, Related Words, Thumbnail, Privacy Status\n';
//
//     videos.forEach(function (video) {
//         str += '"' + video.id + '"';
//         str += ',"' + video.title + '"';
//         str += ',"' + video.keywords + '"';
//         str += ',"' + video.related_words + '"';
//         str += ', ' + video.thumbnail;
//         str += ', ' + video.privacyStatus;
//         str += '\n';
//     });
//     return str;
// }

exports.listAllKeywords = function(req, res) {
    var skip = req.query.skip,
        limit = req.query.limit,
        skipParam = skip ? { $skip: parseInt(skip) } : null,
        limitParam = limit ? { $limit: parseInt(limit) } : null,
        queryParams = [
            {
                $unwind: "$keywords"
            },
            {
                $group: {
                    _id: "$keywords",
                    count: {
                        $sum: 1
                    }
                }
            },
            {
                $sort: {
                    _id: 1
                }
            }];
    //
    //
    if (skipParam) {
        queryParams.push(skipParam);
    }

    if (limitParam) {
        queryParams.push(limitParam);
    }

    queryParams.push({
        $project: {
            _id: 0,
            keyword:'$_id',
            count:'$count'
        }
    });

    console.log('queryParams = ' + queryParams);
    //res.send(queryParams);

    Video.aggregate(queryParams, function (err, keywords) {
            if (!err) {
                res.jsonp(keywords);
            }
            else {
                res.send('Error attempting to list all keywords');
            }
        });
}

// utility functions

function getClientIp(req) {
  var ipAddress;
  // Amazon EC2 / Heroku workaround to get real client IP
  var forwardedIpsStr = req.header('x-forwarded-for');
  if (forwardedIpsStr) {
    // 'x-forwarded-for' header may return multiple IP addresses in
    // the format: "client IP, proxy 1 IP, proxy 2 IP" so take the
    // the first one
    var forwardedIps = forwardedIpsStr.split(',');
    ipAddress = forwardedIps[0];
  }
  if (!ipAddress) {
    // Ensure getting client IP address still works in
    // development environment
    ipAddress = req.connection.remoteAddress;
  }

  return ipAddress;
}
