/**
 * Interfacing with Google API
 */
var util = require('util');

var merge = require('merge');
var fs = require('fs');

var auth = require('./auth.js');
var googleapis = require('googleapis');
var Video = require('../model/video.js');

exports.checkIfChannelIDIsOneOfHarleysChannels = function(channelId) {
	var ssdID = "UCACxqsL_FA-gMD2fwil7ZXA"; //SCOTT: You maybe need to change the Ids here if you want to use
	var sslID = "UCWswiVuY9bs8Pfv9wSt9T8Q"; //an another channel to use the Update,Delete, and Create ends points.

	if(channelId == ssdID || channelId == sslID) {
		return true;
	}

	return false;
}

// googleapis v1.0
var youtube = googleapis.youtube({ version: 'v3', auth: auth.client });
var plus = googleapis.plus({ version: 'v1', auth: auth.client });

exports.youtube = youtube;
exports.plus = plus;

function isEmpty(map) {
   for(var key in map) {
	  if (map.hasOwnProperty(key)) {
		 return false;
	  }
	}
	return true;
}

exports.getVideoDetails = function(callback) {
	youtube.videos.list({
		part: 'snippet, status',
		id: vid,
		fields: 'items/snippet'
	}, callback);
}

exports.printVideoDetails = function(err, video) {
	if (err) {
		console.log('An error occurred: ' + err);
	} else {
		console.log('video = ' + util.inspect(video, {depth:null}));
	}
}

exports.deleteVideo = function(req, res) {
	// if (!auth.isAuthorized()) {
	// 	console.log("user was missing credentials");
	// 	res.sendStatus(401);
	// 	return;
	// }

	exports.getUserChannelID(function(err, channelId) {
		if (!err) {
			if (!exports.checkIfChannelIDIsOneOfHarleysChannels(channelId)) {
				console.log("user that was not one of harley's tried to delete a video :( channelId = " + channelId);
				res.sendStatus(401);
				return;
			}

			var videoID = req.params.id;

			youtube.videos.delete({
				id: videoID,
				auth: auth.client
			}, function (err, result) {
				//if (err) {
				//	console.log('Error: ' + util.inspect(err, {depth:null}));
				//	res.sendStatus(err.code);
				//	return;
				//}

                if (err) {
                    if (err.errors[0].reason !== 'videoNotFound') {
                        console.log('Error: ' + util.inspect(err, {depth:null}));
                        res.sendStatus(err.code);
                        return;
                    }
                }

				Video.findOne({id: videoID}).remove(function (err) {
					if (err) {
						console.log(err);
						res.sendStatus(err.code); //TODO that refresh needs to be run
						return;
					}
				});

				res.sendStatus(200);
				return;
			});
		}
	});
}

exports.getUserChannelID = function(callback) {
	// if (!auth.isAuthorized()) {
	// 	console.log("user was missing credentials");
	// 	res.sendStatus(401);
	// 	//return '';
	// 	callback(true, '');
	// }

	youtube.channels.list({
		part: 'snippet', mine: true
	}, function(err, result) {
		if(err) {
			console.log("Error occur while trying to determine user's channel" + err);
			//res.sendStatus(500);
			//return '';
			callback(true, '');
		}

		var item = result.items[0];
		if(!item) {
			//return '';
			callback(true, '');
		}

		//return item.id;
		callback(false, item.id);
	});
}

exports.updateVideoMetadata = function(req, res) {
	// if (!auth.isAuthorized()) {
	// 	console.log("user was missing credentials");
	// 	res.sendStatus(401);
	// 	return '';
	// }

	// console.log('req.body = ', req.body);

	youtube.videos.update({
		part: 'snippet, status',
		resource: req.body
	}, function (err, result) {
		if (err) {
			console.log('Error occurred while trying to update video on YouTube. Error Message:\n');
			console.log(err);
			res.sendStatus(500);
			return;
		}

		var item = result;

        // console.log('video.item = ' + util.inspect(item, {depth:null}));

        exports.saveVideoMetadataToDB(res, item);
	});
}

exports.saveVideoMetadataToDB = function(res, item) {
	if(!item) {
		res.sendStatus(400);
		return;
	}
	var description = item.snippet.description;
	var keyRelatedWordsJSON = description.slice(description.indexOf("{"), description.lastIndexOf("}") + 1);
	var relatedWords = [];
	var keywords = [];

	if (keyRelatedWordsJSON && keyRelatedWordsJSON != '') {
		try {
			keyRelatedWords = JSON.parse(keyRelatedWordsJSON);
			relatedWords = keyRelatedWords.related_words || [];
			keywords = keyRelatedWords.keywords || [];
		}
		catch (e) {
			console.log('Trying to parse invalid JSON: ' + relatedWords);
		}
	}

	if(!exports.checkIfChannelIDIsOneOfHarleysChannels(item.snippet.channelId)) {
		console.log("user that was not one of harley's tried to delete a video :( channelId = " + item.snippet.channelId);
		res.sendStatus(401);
		return;
	}
	
	var video = {
		id : item.id,
		title : item.snippet.title,
		description : item.snippet.description,
		thumbnail : item.snippet.thumbnails.default.url,
		thumbnailMedium : item.snippet.thumbnails.medium ? item.snippet.thumbnails.medium.url : '',
		thumbnailHigh : item.snippet.thumbnails.high ? item.snippet.thumbnails.high.url : '',
		thumbnailStandard : item.snippet.thumbnails.standard ? item.snippet.thumbnails.standard.url : '',
		keywords : (item.snippet.tags) ? item.snippet.tags.map(function(tag) { return tag.toLowerCase(); }) : [],
        privacyStatus : item.status.privacyStatus,
		related_words : relatedWords
		//channel_id : item.snippet.channelId TODO add channel_id to videos schema
	};
	//printResult(null, video);

	Video.findOne({ id: video.id }, function(error, vid) {
		//console.log('Video.findOne id = ' + video.id);
		if (error) throw error;
		if (vid) {
			//console.log('Found video ' + vid.title + '!!!');
			vid.title = video.title;
			vid.description = video.description;
			vid.keywords = video.keywords;
			vid.related_words = video.related_words;
			vid.thumbnail = video.thumbnail;
			vid.thumbnailMedium = video.thumbnailMedium;
			vid.thumbnailHigh = video.thumbnailHigh;
			vid.thumbnailStandard = video.thumbnailStandard;
            vid.privacyStatus = video.privacyStatus;
		}
		else {
			//console.log('!!!! Creating new Video for ' + video.title);
			vid = new Video(video);
		}
		vid.save(function (savingError) {
			if (savingError) throw savingError;
			//console.log('Video ' + vid.title + ' saved.');
			//console.log(vid);
			res.status(200).jsonp(video);
			return;
		});
	});
}

exports.saveVideoMetadata = function(req, res) {
	// if (auth.isAuthorized()) {

        console.log('saveVideoMetadata');

		youtube.videos.list({
			id: req.params.id,
			part: 'snippet, status',
		}, function(err, result) {
			if (err) {
				console.log('Error occurred while trying to retrieve video from YouTube. Error Message:' + err);
				res.sendStatus(500);
				return;
			}

			var item = result.items[0];

            //console.log('video.item: ' + util.inspect(item, {depth:null}));

			exports.saveVideoMetadataToDB(res, item);
		});
	// }
	// else {
	// 	console.log("User was missing credentials");
	// 	res.sendStatus(401);
	// 	return;
	// }
}

exports.setThumbnail = function(req, res, videoId, filename, callback) {
    console.log('gapi.setThumbnail ' + filename);

    // if (auth.isAuthorized()) {
        youtube.thumbnails.set({
            videoId: videoId,
            media: {
                mimeType: 'image/jpeg',
                body: fs.createReadStream(filename).on('error', function(err) {
                    console.log('Error in gapi.setThumbnail createReadStream: ' + err.message);
                    //res.end(err);
                    return;
                })
            }
        }, function(err, result) {
            if (err) {
                console.log('Error in gapi.setThumbnail youtube.thumbnails.set: ' + filename + ', ' + videoId + ': ' + err);
                //res.sendStatus(500);
                return;
            }
            callback();
        });
    // }
    // else {
    //     console.log("User was missing credentials");
    //     res.sendStatus(401);
    //     return;
    // }
}