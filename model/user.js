var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var userSchema = new Schema({
	name: String,
	email: String,
	active: Boolean,
	studies: [{ type: Schema.Types.ObjectId, ref: 'Study' }], // list of studies this user participated in
	timestamp: { type: Date, default: Date.now }
});

module.exports = mongoose.model('User', userSchema);