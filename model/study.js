var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var studySchema = new Schema({
	name: String,
	notes: String,
	start_date: { type: Date },
	end_date: { type: Date },
	num_participants: Number,
	participants: [{ type: Schema.Types.ObjectId, ref: 'User' }]
});

module.exports = mongoose.model('Study', studySchema);