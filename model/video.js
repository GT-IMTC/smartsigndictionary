var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var videoSchema = new Schema({
	id: { type: String, required: true },
	title: { type: String, default: '' },
	description: { type: String, default: '' },
    thumbnail: { type: String, default: '' },
    thumbnailMedium: { type: String, default: '' },
    thumbnailHigh: { type: String, default: '' },
    thumbnailStandard: { type: String, default: '' },
	keywords: { type: Array },
	related_words: { type: Array },
    privacyStatus: { type: String, default: 'public'}
}, { autoIndex: true });

videoSchema.index({ id: 1 });
videoSchema.index({ keywords: 1 });
videoSchema.index({ title: 1 });

module.exports = mongoose.model('video', videoSchema);