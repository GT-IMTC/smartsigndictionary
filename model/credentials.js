var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var credentialsSchema = new Schema({
	channel_id: { type: String, required: true },
	tokens: Schema.Types.Mixed
	//access_token: { type: String, default: null },
	//refresh_token: { type: String, default: null }
});

module.exports = mongoose.model('Credentials', credentialsSchema);