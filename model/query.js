var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var querySchema = new Schema({
	query: String,
	found: Boolean,
	ip_address: String,
	user: { type: Schema.Types.ObjectId, ref: 'User' },
	study: { type: Schema.Types.ObjectId, ref: 'Study' },
	timestamp: { type: Date, default: Date.now }
}, { autoIndex: true });

querySchema.index({ timestamp: -1 });

module.exports = mongoose.model('Query', querySchema);