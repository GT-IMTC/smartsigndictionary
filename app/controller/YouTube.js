/*
 * File: app/controller/YouTube.js
 *
 * This file was generated by Sencha Architect version 3.2.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Sencha Touch 2.3.x library, under independent license.
 * License of Sencha Architect does not include license for Sencha Touch 2.3.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('SmartSign.controller.YouTube', {
    extend: 'Ext.app.Controller',
    alias: 'controller.youtube',

    config: {
        views: [
            'YouTubePlayer'
        ],

        refs: {
            youTubePlayer: 'youtubeplayer'
        },

        control: {
            "youtubeplayer": {
                youtubeiframeapiready: 'onYouTubeIframeApiReady'
            },
            "mainpanel button[action=back]": {
                tap: 'onBackButtonTap'
            }
        }
    },

    onYouTubeIframeApiReady: function(panel, eventOptions) {
        console.log('YouTube Controller: YouTube iFrame API is Ready!');
    },

    onBackButtonTap: function(button, e, eOpts) {
        this.stopVideo();
    },

    onYouTubePlayerStateChange: function(event) {
        switch (event.data) {
            case YT.PlayerState.PLAYING:
                console.log('Now Playing video');
                break;

            case YT.PlayerState.PAUSED:
                console.log('Video paused.');
                break;

            case YT.PlayerState.BUFFERING:
                console.log('Video buffering');
                break;

            case YT.PlayerState.CUED:
                console.log('Video cued');
                break;

            case YT.PlayerState.ENDED:
                console.log('Video ended');
                break;

        }

    },

    stopVideo: function() {
        var player = this.getYouTubePlayer().getPlayer();

        if (player) player.stopVideo();

    }

});