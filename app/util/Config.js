Ext.define('SmartSign.util.Config', {
    singleton : true,

    config : {
        noLog: false,
        //dbUrl: 'http://localhost:8080'
        // dbUrl: 'http://smartsign.imtc.gatech.edu',
        baseUrl: window.location.protocol + '//' + window.location.hostname
    },

    constructor : function(config) {
        this.initConfig(config);
        this.callParent([config]);
    }
});