#!/bin/env node

// var Decompress = require('decompress');
var fs = require('fs');
var rimraf = require('rimraf');

var express = require('express');
var bodyParser     = require('body-parser');
var methodOverride = require('method-override');
var util = require('util');
var app = express();

// client sessions
var sessions = require("client-sessions");

var multer  = require('multer');

// establish MongoDB connection
var db = require('./controller/db.js');

//  Get the environment variables we need.
var ipaddr  = process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0';
var port    = process.env.OPENSHIFT_NODEJS_PORT || '8080';
// var ipaddr  = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';
// var port    = process.env.OPENSHIFT_NODEJS_PORT || '8083';

// Express 4.0
app.use(express.static(__dirname + '/build/testing/SmartSign'));
// app.use(express.static(__dirname + '/smartsign/build/production/SmartSign'));
app.use(express.static(__dirname + '/admin_tools'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(methodOverride());

app.use(sessions({
    cookieName: 'session', // cookie name dictates the key name added to the request object
    secret: ',TPrwQhDAP8b9waRwyMBrkiPppWeVxifqM{', // should be a large unguessable string
    duration: 30 * 60 * 1000, // how long the session will stay valid in ms
    activeDuration: 1000 * 60 * 5, // if expiresIn < activeDuration, the session will be extended by activeDuration milliseconds
    httpOnly: true,
    secure: true,
    ephemeral: true
}));

app.set('views', __dirname + '/admin_tools');
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');

// set up the RESTful API, handler methods are defined in api.js
var api = require('./controller/api.js');
var auth = require('./controller/auth.js');
var google_api = require('./controller/gapi.js');/*Configure the multer.*/

// var done = false;

/*
app.use(multer({ dest: './uploads/',
    rename: function (fieldname, filename) {
        console.log('renaming file ' + filename);
        return filename + '_' + Date.now();
    },
    onFileUploadStart: function (file, req, res) {
        console.log(file.originalname + ' upload is starting ...');
        done = false;
    },
    onFileUploadComplete: function (file, req, res) {
        console.log(file.fieldname + ' uploaded to ' + file.path);
        done = true;
    }
}));
*/

// app.use(function(req, res, next) {
//     if (req.session && req.session.credentials) {
//         auth.client.setCredentials(req.session.credentials);
//     }
//     next();
// });

// routes

app.get('/', function (req, res) {
	// res.redirect('/index.html');
	res.sendFile(path.join(__dirname + '/index.html'));
});



// app.get('/admin', auth.requireAuthorization, auth.getUserName, function(req, res) {
app.get('/admin', auth.requireAuthorization, auth.getUserName, function(req, res) {
// app.get('/admin', auth.requireAuthorization, function(req, res) {

    res.render('admin.html', {
        token : req.session.credentials, //auth.client.credentials,
        userName : req.session.currentUser //auth.currentUser
    });

/*
	if (req.query.code) { // && !auth.isAuthorized()) {
		// if we are coming back from Google's redirect
		console.log('Returning from Google oauth2 redirect');
		auth.getToken(req.query.code, function(err, token) {
			if (err) {
				console.log('Error setting oauth2 token');
				// auth.client.credentials = null;
				res.redirect(auth.url);
			} else {
				console.log('Google oauth2 setToken successful');

                // save credentials in client session
                req.session.credentials = token;

				auth.getUserName(function(err){
					if (err) {
						auth.client.credentials = null;
						console.log('server: Auth error, redirecting to ' + auth.url);
                        res.send('Error: Unauthorized Google User. Please log out of your Google account and retry.');
						//res.redirect(auth.url);
					} else {

						res.render('admin.html', {
							token : auth.client.credentials,
							//token : google_api.client.credentials,
							userName : auth.currentUser
						});
					}
				});
			}
		});
	} else {
		// check if we have a token already
		if (auth.isAuthorized()) {
			console.log('server: client.credentials = ', auth.client.credentials);
			auth.getUserName(function(err) {
				if (err) {
					auth.client.credentials = null;
					res.redirect(auth.url);
				} else {
					res.render('admin.html', {
						token : auth.client.credentials,
						userName : auth.currentUser
					});
				}
			});
		} else {
			auth.client.credentials = null;
			// else we redirect to get a token
			res.redirect(auth.url);
		}
	}
	*/
});

app.get('/authDirect', auth.changeUser);

app.get('/queries/search/:keyword?', api.searchQueries);
app.get('/queries', api.listQueries);

app.post('/query/:query/:found/:ip_address/:user/:study?', api.saveQuery);
app.post('/queries', api.saveQuery);
app.get('/queries/:_id.:format?', api.showQuery);
//app.get('/queries', api.listQueries);
app.get('/queries.:format?', api.listQueriesDateRange);
app.put('/queries/:_id', api.saveQuery);

app.get('/videos', api.listVideos);
app.get('/videos/search/:keyword?', api.searchVideos);
// app.get('/listallvideos', api.listAllVideos);
app.get('/videos/search', api.findVideosByKeyword);
app.get('/videos.:format?', api.listAllVideos);
app.get('/videos/:id', api.showVideoById);
app.post('/videos/:id', auth.requireAuthorization, google_api.saveVideoMetadata);
app.put('/videos/:id', auth.requireAuthorization, google_api.updateVideoMetadata);
app.delete('/videos/:id', auth.requireAuthorization, google_api.deleteVideo);
app.post('/videoshardreset', auth.requireAuthorization, api.hardResetVideos);

app.get('/keywords', api.listAllKeywords);

/*
app.post('/thumbnails', function(req, res, next) {
    console.log('Trying to upload a file');
    if (req.files) {
        console.log(util.inspect(req.files));
        if (req.files.myFile.size === 0) {
            return next(new Error("Hey, first would you select a file?"));
        }
        fs.exists(req.files.myFile.path, function(exists) {
            if(exists) {
                res.end("Got your file!");
            } else {
                res.end("Well, there is no magic for those who don’t believe in it!");
            }
        });
    }
});
*/

/* Handling file upload routes.*/

/*
var fs = require('fs'),
    csv = require('csv'),
    parse = csv.parse;

app.post('/thumbnails',[ multer({ dest: __dirname + '/uploads/',
    putSingleFilesInArray: true,

    rename: function (fieldname, filename) {
        //console.log('renaming file ' + filename);
        return filename + '_' + Date.now();
    },

    onFileUploadStart: function (file, req, res) {
        //console.log(file.originalname + ' is starting ...');

        done = false;

        //if (file.mimetype !== 'application/x-gzip') {
        if (file.mimetype !== 'application/zip' && file.mimetype !== 'application/octet-stream' && file.mimetype !== 'application/x-zip-compressed') {
            console.log('Error: Attempting to upload file which is not a .zip archive! mimetype = ' + file.mimetype);
            return false;
        }
    },

    onFileUploadComplete: function (file, req, res) {
        var filename = file.path;

        //console.log(file.fieldname + ' uploaded to  ' + file.path);
        done = true;

        // remove any previous files from thumbnails folder
        rimraf(__dirname + '/thumbnails/*', function(err) {
            if (err) {
                throw err;
            }
        });

        // unzip file
        var decompress = new Decompress({mode: '755'})
            .src(filename)
            .dest(__dirname + '/thumbnails/')
            .use(Decompress.zip({ strip: 1 }));

        decompress.run(function (err) {
            if (err) {
                throw err;
            }

            console.log('Archive extracted successfully!');

            // delete .zip file
            fs.unlink(filename, function(err) {
                if (err) {
                    throw err;
                }
            });

            var parser = parse({ delimiter: ',', columns: true });

            // Use the writable stream api
            parser.on('readable', function() {
                var record,
                    videoId,
                    thumbnailFilename;

                while(record = parser.read()){
                    console.log('Uploading ' + util.inspect(record));
                    if (record.Thumbnail && record.Thumbnail.slice(-4) === '.jpg' || record.Thumbnail.slice(-4) === '.png') {
                        videoId = record["YouTube ID"];
                        thumbnailFilename = __dirname+'/thumbnails/' + record.Thumbnail;
                        //console.log(videoId + ', ' + thumbnailFilename);

                        // upload thumbnails to Youtube
                        google_api.setThumbnail(req, res, videoId, thumbnailFilename, function() {
                            console.log('Done uploading thumbnail ' + thumbnailFilename + ' for video ' + videoId);
                        });
                    }
                    else {
                        console.log('Error attempting to upload custom thumbnail: "' + record.Thumbnail + '"');
                    }

                    //output.push(record);
                    //if (record[4] && record[4].slice(-4) === '.jpg' || record[4].slice(-4) === '.png') {
                    //    videoId = record[0];
                    //    thumbnailFilename = __dirname+'/thumbnails/'+record[4];
                    //    //console.log(videoId + ', ' + thumbnailFilename);
                    //
                    //    // upload thumbnails to Youtube
                    //    google_api.setThumbnail(req, res, videoId, thumbnailFilename, function() {
                    //        console.log('Done uploading thumbnail ' + thumbnailFilename + ' for video ' + videoId);
                    //    });
                    //}
                    //else {
                    //    console.log('Error attempting to upload custom thumbnail: ' + record[4]);
                    //}
                }
            });

            // Catch any error
            parser.on('error', function(err){
                console.log(err.message);
            });

            // When we are done, test that the parsed output matched what expected
            parser.on('finish', function() {
                //console.log('Finished parsing thumbnails CSV');
            });

            var rs = fs.createReadStream(__dirname+'/thumbnails/thumbnails.csv');

            rs.on('error', function(err) {
                console.log(err.message);
                res.end(err);
            });

            rs.pipe(parser);
        });

    }}), function(req, res) {
        if (done === true) {
            console.log(req.body) // form fields
            console.log(req.files) // form files
            //res.status(204).end();
            //res.send('Upload done!!!');

            res.json({success: true, status: 200});
        }
        else {
            res.json({success: false, status: 502});
        }
    }]
);
*/

//  And start the app on that interface (and port).
app.listen(port, ipaddr, function() {
    // process.setuid(33);
    console.log('Server\'s UID is now ' + process.getuid());
	console.log('%s: SmartSign Node server started on %s:%d ...', Date(Date.now() ), ipaddr, port);
});
